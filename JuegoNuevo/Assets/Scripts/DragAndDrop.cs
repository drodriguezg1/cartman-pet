using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{

    public CartmanController ensuciar;
    bool canMove;
    bool dragging;

    private new Collider2D collider = new Collider2D();
    private double tiempo = 0;
    void Start()
    {
        collider = GetComponent<Collider2D>();
        canMove = false;
        dragging = false;

    }

    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (this.collider == Physics2D.OverlapPoint(mousePos))
            {
                canMove = true;
            }
            else
            {
                canMove = false;
            }
            if (canMove)
            {
                dragging = true;
            }


        }
        if (dragging)
        {
            this.transform.position = mousePos;
        }
        else
        {
            this.transform.position = new Vector3(-1.8F,-4.30F, 0);
        }
        if (Input.GetMouseButtonUp(0))
        {
            canMove = false;
            dragging = false;
        }
    }

   

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("sucio"))
        {
            tiempo += Time.deltaTime;


            int ola = (int) tiempo;
           
            if (ola == 1) 
            {
                tiempo = 0;
                Debug.Log(ola);
                ensuciar.Limpiar();
            }
              
        }

    }
}
