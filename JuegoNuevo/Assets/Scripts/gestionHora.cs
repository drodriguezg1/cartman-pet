using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class gestionHora : MonoBehaviour
{

    private float segundos = 10;

    public delegate void ensuciar();
    public event ensuciar OnSUcio;

    
    void Update()
    {
       if ((int)segundos == (int)0)
        {
            segundos = 10;
        }
        else
        {
            segundos -= Time.deltaTime;
        }
       if ((int)segundos == (int)0)
        {
            OnSUcio?.Invoke();

        
        }
    }
}
