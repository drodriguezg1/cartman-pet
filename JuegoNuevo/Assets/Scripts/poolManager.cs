using System.Collections.Generic;
using UnityEngine;

public class poolManager : MonoBehaviour
{
    [SerializeField] private GameObject comidabasura;
    [SerializeField] private GameObject lechuga;
    [SerializeField] private GameObject ladrillo;
    [SerializeField] public List<GameObject> prefabList;
    private int poolsize = 5;



    private void Start()
    {
        a�adirPool(poolsize);
    }
     
    private void a�adirPool(int tama�o)
    {
        for (int i = 0; i < tama�o; i++)
        {
            GameObject comidabuena = Instantiate(comidabasura);
            GameObject comidamala = Instantiate(lechuga);
            GameObject objeto = Instantiate(ladrillo);

            comidabuena.SetActive(false);
            prefabList.Add(comidabuena);
            comidabuena.transform.parent = this.transform;

            comidamala.SetActive(false);
            prefabList.Add(comidamala);
            comidamala.transform.parent = this.transform;

            objeto.SetActive(false);
            prefabList.Add(objeto);
            objeto.transform.parent = this.transform;
        }
    }


    public GameObject comidaRequest()
    {

        for (int i = 0; i < prefabList.Count; i++)
        { 
            int r = Random.Range(0, prefabList.Count);

            float pos = Random.Range(-2.88f, 2.75f);    


            if (prefabList[r].activeSelf == false)
            {

                prefabList[r].SetActive(true);
                prefabList[r].transform.position = new Vector3(pos, 5.5f, 0);


                return prefabList[r];


            }
        }
        return null;
    }

}
