using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuntosController : MonoBehaviour
{

    public jugar juego;
    private int puntos = 1;
    [SerializeField] private ScriptableObjecto scriptable;



    private void Awake()
    {
        if (juego)
            juego.OnSumar += sumarPuntos;

    }


    public void sumarPuntos()
    {
        this.GetComponent<TextMeshProUGUI>().text = puntos+"";
        puntos++;
        scriptable.puntos++;

    }
}
