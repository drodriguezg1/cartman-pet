using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ControlEvolucion : MonoBehaviour
{
    [SerializeField] private ScriptableObjecto scriptable;


    private void Awake()
    {
        this.GetComponent<TextMeshProUGUI>().text = scriptable.PuntosEvolucion + " To Evolve";

    }


    public void Cambio()
    {
        this.GetComponent<TextMeshProUGUI>().text = scriptable.PuntosEvolucion + "To Evolve";
    }

}
