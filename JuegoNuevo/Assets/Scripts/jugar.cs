using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.SceneManagement;

public class jugar : MonoBehaviour
{

    [SerializeField]
    private InputActionAsset m_InputAsset;
    
    [SerializeField]
    private ScriptableObjecto scriptable;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private Vector2 pos;
    public float vel;

    public Sprite CartmanAngry;
    public Sprite CartmanAngrySucio;


    private AudioSource m_AudioSource;

    public delegate void QuitarVida();
    public event QuitarVida OnResta;
    
    public delegate void SumarPuntos();
    public event QuitarVida OnSumar;


    // Start is called before the first frame update
    void Awake()
    {
            m_AudioSource = GetComponent<AudioSource>();
            this.GetComponent<SpriteRenderer>().sprite = scriptable.sprite;
            m_Input = Instantiate(m_InputAsset);
            m_MovementAction = m_Input.FindActionMap("default").FindAction("move");
            m_Input.FindActionMap("Default").Enable();     
    }

    // Update is called once per frame
    void Update()
    {
        pos = m_MovementAction.ReadValue<Vector2>();

    }


    private void FixedUpdate()
    {
       this.GetComponent<Rigidbody2D>().velocity = new Vector2(pos.x*vel, 0);
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ladrillo") || collision.CompareTag("lechuga"))
        {
            OnResta?.Invoke();
            m_AudioSource.Play();

            if (this.GetComponent<SpriteRenderer>().sprite.name.Equals("sucio"))
            {
                this.GetComponent<SpriteRenderer>().sprite = CartmanAngrySucio;
                StartCoroutine(tiempo());
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = CartmanAngry;
                StartCoroutine(tiempo());
            }
           
        }

        if (collision.CompareTag("hamburguesa"))
        {

            OnSumar?.Invoke();

        }
    }

    IEnumerator tiempo()
    {


        yield return new WaitForSeconds(1.5f);
        this.GetComponent<SpriteRenderer>().sprite = scriptable.sprite;

    }

}

