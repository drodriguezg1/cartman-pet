using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VidaController : MonoBehaviour
{
    public jugar juego;
    private int vida = 1;
    // Start is called before the first frame update
    private void Awake()
    {
        // spriteRenderer = GetComponent<SpriteRenderer>();
        if (juego)
            juego.OnResta += restarVida;

    }

    private void  restarVida()
    {
        GameObject a;

        ComprovarVida();

        if (vida == 1)
        {
            a = transform.GetChild(transform.childCount - vida).gameObject;
            a.SetActive(false);
            vida++;
        }
        else if (vida == 2)
        {
            a = transform.GetChild(transform.childCount - vida).gameObject;
            a.SetActive(false);
            vida++;
        }
        else if(vida == 3)
        {
            a = transform.GetChild(transform.childCount - vida).gameObject;
            a.SetActive(false);
        }
  
    }

    private void ComprovarVida()
    {

        if (vida == 3)
        {
            SceneManager.LoadScene("Main");
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
