using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class comidaController : MonoBehaviour
{

    public GameObject lechuga;
    public GameObject hamburg;
    public GameObject ladrillo;

    private void Start()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("limpio"))
        {
            if (this.CompareTag("lechuga"))
            {
                if (lechuga)
                {
                    Instantiate(lechuga, this.transform.position, Quaternion.identity);
                    this.gameObject.SetActive(false);
                }
                

            }
            if (this.CompareTag("hamburguesa"))
            {
                if (hamburg)
                {
                    Instantiate(hamburg, this.transform.position, Quaternion.identity);
                    this.gameObject.SetActive(false);
                }
            }
            if (this.CompareTag("ladrillo"))
            {
                if (ladrillo)
                {
                    Instantiate(ladrillo, this.transform.position, Quaternion.identity);
                    this.gameObject.SetActive(false);
                }
            }
        } 
        
        //SOLO ACTIVAR PARTICULAS CUANDO IMPACTA CONTRA EL CARTMAN
        //SOLO ACTIVAR PARTICULAS CUANDO IMPACTA CONTRA EL CARTMAN
        //SOLO ACTIVAR PARTICULAS CUANDO IMPACTA CONTRA EL CARTMAN

        if (collision.CompareTag("suelo"))
        {
            if (this.CompareTag("lechuga"))
            { 
               
                    this.gameObject.SetActive(false);
                
            }
            if (this.CompareTag("hamburguesa"))
            {
                this.gameObject.SetActive(false);
            }
            if (this.CompareTag("ladrillo"))
            {
                this.gameObject.SetActive(false);
            }
        } 
    }
}
