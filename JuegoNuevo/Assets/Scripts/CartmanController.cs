using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartmanController : MonoBehaviour
{
    [SerializeField] private ScriptableObjecto scriptable;

    public gestionHora ensuciarHora;
    public Sprite Sucio;
    public Sprite Limpio;
    private float y = 0.2068116f;
    private float x = 0.13f;

    public ControlEvolucion controlevo;
    public ModificarPuntos modevo;

    public delegate void cambiar();
    public event cambiar OnCambio;

    // Start is called before the first frame update
    private void Awake()
    {
        // spriteRenderer = GetComponent<SpriteRenderer>();
        if (ensuciarHora)
        { 
            ensuciarHora.OnSUcio += Ensuciar;
        }

       this.transform.localScale = new Vector3(scriptable.CartmanX, +scriptable.CartmanZ, 0);
       this.GetComponent<SpriteRenderer>().sprite = scriptable.sprite;

        if (this.GetComponent<SpriteRenderer>().sprite.name.Equals("sucio"))
        {
            this.transform.tag = "sucio";
        }
        else this.transform.tag = "limpio";

    }

    public void Limpiar()
    {
        
        if (this.tag == "sucio")
        {

            this.GetComponent<SpriteRenderer>().sprite = Limpio;
            this.transform.tag = "limpio";
        }
    }

    public void Ensuciar()
    {
        if (this.tag == "limpio")
        {

            this.GetComponent<SpriteRenderer>().sprite = Sucio;
            this.transform.tag = "sucio";
        }
    }

    public void Engordar()
    {
        if (scriptable.puntos >= scriptable.PuntosEvolucion)
        {
            transform.localScale = new Vector3(x+0.1f, y, 0);
            scriptable.puntos -= scriptable.PuntosEvolucion;
            scriptable.PuntosEvolucion += 10;
            controlevo.Cambio();
            modevo.Cambio();
        }
    }

    public void FixedUpdate()
    {
        x = this.transform.localScale.x;
        scriptable.sprite = this.GetComponent<SpriteRenderer>().sprite;
        scriptable.CartmanX = x;
        scriptable.CartmanZ = y;

    }

    public void OnDestroy()
    {
        ensuciarHora.OnSUcio -= Ensuciar;

    }
}
