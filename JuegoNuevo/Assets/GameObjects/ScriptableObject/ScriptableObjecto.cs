using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableObjecto : ScriptableObject
{
    public int puntos;
    public int PuntosEvolucion = 10;
    public float CartmanX;
    public float CartmanZ;
    public Sprite sprite;
}
